<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S05: Client-Server Communication (Basic To-Do List)</title>
	</head>
	<body>
		<?php session_start(); ?>

		<?php if(!isset($_SESSION['email'])): ?>
		<h3>Sign In</h3>
			<form method="POST" action="./server.php">
				<!-- This will identify the action or type of request of a user. -->
				<input type="hidden" name="action" value="login" />
				Email: <input type="email" name="email" required style="display: inline-block"/>
				Password: <input type="password" name="password" required style="display: inline-block"/>
				<button type="submit">Login</button>
			</form>
			<?php if(isset($_SESSION['login_error_message'])): ?>
				<p><?php echo $_SESSION['login_error_message']?></p>
			<?php endif;?>
		<?php endif;?>

		<br>
		<?php if(isset($_SESSION['email'])): ?>
			<h1><?php echo $_SESSION['email']?></h1>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="logout" />
				<button type="submit">Logout</button>
			</form>
		<?php endif;?>
	</body>
</html>