<?php

    session_start();
    // echo $_POST['description'];
    // echo $_SESSION['greet'];

    class UserLogin{
        public function login($email,$password){
            if($email !== "test@test.com" || $password !== "password"){
                if($_SESSION['login_error_message'] === null) {
                    $_SESSION['login_error_message'] = "Incorrect username or password";
                }
            } else {
                if($_SESSION['email'] === null) {
                    $_SESSION['email'] = "test@test.com";
                }
                if($_SESSION['login_error_message'] !== null) {
                    $_SESSION['login_error_message'] = null;
                }
            }
        }
        public function logout(){
            session_destroy();
        }
    }

    $user = new UserLogin();

    if($_POST['action'] === 'login'){
        $user->login($_POST['email'],$_POST['password']);
    } else if($_POST['action'] === 'logout'){
        $user->logout();
    }

    header('Location: ./index.php');


?>
