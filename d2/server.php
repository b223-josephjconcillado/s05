<?php

    session_start();
    // echo $_POST['description'];
    // echo $_SESSION['greet'];

    class TaskList{
        // add task
        public function add($description){
            $newTask = (object) [
                'description' => $description,
                'isFinished' => false
            ];
            // if there is no added task yet
            if($_SESSION['tasks'] === null){
                $_SESSION['tasks'] = array();
            } 
            // then $newTask will be added in the $_SESSION['tasks'] variable.
            array_push($_SESSION['tasks'], $newTask);
        }
        public function update($id,$description,$isFinished){
            $_SESSION['tasks'][$id]->description = $description;
            $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
        }
        public function remove($id){
            // array_splice(array,startDel,length,newArrElement)
            array_splice($_SESSION['tasks'], $id,1);
        }
        public function clear(){
            session_destroy();
        }
    }

    $taskList = new TaskList();

    if($_POST['action'] === 'add'){
        $taskList->add($_POST['description']);
    } else if($_POST['action'] === 'update'){
        $taskList->update($_POST['id'],$_POST['description'],$_POST['isFinished']);
    } else if($_POST['action'] === 'remove'){
        $taskList->remove($_POST['id']);
    } else if($_POST['action'] === 'clear'){
        $taskList->clear();
    }

    header('Location: ./index.php');


?>
